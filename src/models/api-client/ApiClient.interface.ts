import { ItemsApiClientInterface } from './items/item';

export interface ApiClientInterface {
  items: ItemsApiClientInterface;
}
