import { ItemsApiClientInterface, ItemsApiClientUrlsInterface, ItemsApiClientModel } from '@/models/api-client/items/item.ts';

const urls: ItemsApiClientUrlsInterface = {
  fetchItems: '/static/data/items.json'
};

const itemsApiClient: ItemsApiClientInterface = new ItemsApiClientModel(urls);

export default itemsApiClient;
