import { ItemsApiClientUrlsInterface, ItemsApiClientInterface, ItemsApiClientModel } from '@/models/api-client/items/item.ts';
const urls: ItemsApiClientUrlsInterface = {
  // TODO : remplacer par une URL
  fetchItems: '/static/data/items.json'
};

const itemsApiClient: ItemsApiClientInterface = new ItemsApiClientModel(urls);

export default itemsApiClient;
