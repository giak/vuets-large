module.exports = {
    trailingComma: 'none',
    tabWidth: 2,
    useTabs: false,
    semi: true,
    singleQuote: true,
    bracketSpacing: true,
    arrowParens: 'always',
    proseWrap: 'preserve',
    printWidth: 150,
    vueIndentScriptAndStyle: false
};
